<?php
namespace app\common\constant;

class Status {
    const DELETE = -1;
    const OFF = 0;
    const ON = 1;
}