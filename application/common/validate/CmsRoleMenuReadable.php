<?php
namespace app\common\validate;
use think\Validate;
class CmsRoleMenuReadable extends Validate {
    protected $rule = [
        'role_id' => 'require|integer',
        'menu_id' => 'require|integer',
    ];
}