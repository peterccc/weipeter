<?php
namespace app\common\validate;
use think\Validate;

class FormToken extends Validate {
    protected $rule = [
        '__token__' => 'require|token'
    ];

    protected $message = [
        '__token__.require' => '非法提交！',
        '__token__.token' => '请勿重复提交！'
    ];
}