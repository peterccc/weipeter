<?php
namespace app\common\validate;
use think\Validate;
class CmsUser extends Validate {
    protected $rule = [
        'username' => 'require|max:32|unique:CmsUser',
        'password' => 'require|max:32',
        'nickname' => 'max:32',
        'desc' => 'max:255',
        'role_id' => 'require|integer',
        'advanced_manager' => 'require|boolean',
        'status' => 'integer',
        'create_time' => 'integer'
    ];

    protected $message = [
        'username.max' => '用户名不能超过32位'
    ];
}