<?php
namespace app\common\validate;
use think\Validate;
class CmsRoleAuth extends Validate {
    protected $rule = [
        'role_id' => 'require|integer',
        'action_id' => 'require|integer',
    ];
}