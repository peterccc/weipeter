<?php
namespace app\common\validate;
use think\Validate;
class CmsRole extends Validate {
    protected $rule = [
        'name' => 'require|max:255|unique:CmsRole',
        'desc' => 'max:255',
    ];
}