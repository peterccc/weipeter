<?php
namespace app\common\session;

class CmsUserSession {
    /**
     * @return \app\admin\model\User
     */
    public static function getUser() {
        return session('user');
    }

    /**
     * @param $user \app\admin\model\User
     */
    public static function setUser($user) {
        session('user', $user);
    }

}