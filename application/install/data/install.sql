CREATE TABLE IF NOT EXISTS `onepeter_cms_img`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `type` int NOT NULL , /*1:local 本地服务器中的, 2:external 外面其他服务器中的*/
  `path` VARCHAR (255) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `onepeter_cms_file`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `type` int NOT NULL , /*1:local 本地服务器中的, 2:external 外面其他服务器中的*/
  `path` VARCHAR (255) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `onepeter_cms_role`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `name` VARCHAR(255) NOT NULL UNIQUE ,
  `desc` VARCHAR(255) NOT NULL DEFAULT ''
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `onepeter_cms_role` VALUES(1, 'admin', '后台管理员角色，此角色请勿删除');

CREATE TABLE IF NOT EXISTS `onepeter_cms_user`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `username` VARCHAR(32) NOT NULL UNIQUE ,
  `password` CHAR (32) NOT NULL,
  `nickname` VARCHAR(32) NOT NULL DEFAULT '',
  `desc` VARCHAR(255) NOT NULL DEFAULT '',
  `role_id` INT UNSIGNED NOT NULL,
  `advanced_manager` BOOLEAN NOT NULL DEFAULT FALSE ,
  `status` TINYINT(3) NOT NULL DEFAULT '1',
  `create_time` INT UNSIGNED NOT NULL,
  foreign key (`role_id`) references `onepeter_cms_role` (`id`) on delete cascade
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `onepeter_cms_user` VALUES(1, '{ROOT}', md5('{PASSWORD}'), DEFAULT, '后台管理员', 1,true, 1, 0);

CREATE TABLE IF NOT EXISTS `onepeter_cms_role_auth`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `role_id` INT UNSIGNED NOT NULL,
  `action_id` INT NOT NULL,
  foreign key (`role_id`) references `onepeter_cms_role` (`id`) on delete cascade
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `onepeter_cms_role_menu_readable`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `role_id` INT UNSIGNED NOT NULL,
  `menu_id` INT NOT NULL,
  foreign key (`role_id`) references `onepeter_cms_role` (`id`) on delete cascade
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `onepeter_config` (
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `name` VARCHAR (100) NOT NULL UNIQUE ,
  `value` TEXT,
  `option` TEXT /*用json来配置其他选项配置*/
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;



CREATE TABLE IF NOT EXISTS `onepeter_mp`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `alias` VARCHAR(50) NOT NULL ,  /*微信公众号的昵称，用来说明公众号的作用*/
  `debug` TINYINT UNSIGNED NOT NULL DEFAULT 1,  /*1表示true，0表示false*/
  `originid` VARCHAR (50) NOT NULL ,
  `appid` VARCHAR (50) NOT NULL ,
  `appsecret` VARCHAR (50) NOT NULL ,
  `token` VARCHAR (50) NOT NULL ,
  `interface_token` VARCHAR (50) NOT NULL ,
  `create_time` INT UNSIGNED NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `onepeter_mp_reply`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `keyword` VARCHAR(50) UNIQUE ,
  `type` TINYINT NOT NULL , /*1=文本内容,2=让模块来处理*/
  `content` TEXT NOT NULL /*type=1 则这里是文本内容，type=2 则这里是模块名*/
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;