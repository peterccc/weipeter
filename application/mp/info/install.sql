/**
用户留言，指在公众号发消息，未经过模块处理的
 */
CREATE TABLE IF NOT EXISTS `onepeter_mp_message`(
  `id` INT UNSIGNED PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `mpid` INT UNSIGNED NOT NULL ,
  `openid` VARCHAR (50) NOT NULL ,
  `type` TINYINT UNSIGNED NOT NULL, /*消息类型，有可能文本，图片，语音*/
  `content` TEXT NOT NULL,
  `create_time` INT UNSIGNED NOT NULL,
  FOREIGN KEY (`mpid`) REFERENCES `onepeter_mp`(`id`) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


/**
二维码
 */
CREATE TABLE IF NOT EXISTS `onepeter_mp_qrcode`(
  `id` INT UNSIGNED PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `mpid` INT UNSIGNED NOT NULL ,
  `scene_name` VARCHAR (255) NOT NULL ,
  `type` TINYINT UNSIGNED NOT NULL , /*二维码类型，1为临时二维码，2为永久二维码id，3为永久二维码str*/
  `scene_id` INT UNSIGNED, /*场景id*/
  `scene_str` VARCHAR (255), /*场景字符串*/
  `expire` INT UNSIGNED , /*过期时间*/
  `ticket` VARCHAR(255) ,
  `url` VARCHAR (255),
  `short_url` VARCHAR (255),
  `keyword`VARCHAR (255),
  `create_time` INT UNSIGNED NOT NULL,
  FOREIGN KEY (`mpid`) REFERENCES `onepeter_mp`(`id`) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `onepeter_mp_qrcode_statistics`(
  `id` INT UNSIGNED PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `mpid` INT UNSIGNED ,
  `openid` VARCHAR (255) ,
  `scene_name` VARCHAR (255) ,
  `keyword` VARCHAR (255),
  `scene_type` TINYINT UNSIGNED,
  `scene_id` INT UNSIGNED,
  `scene_str` VARCHAR (255),
  `create_time` INT UNSIGNED,
  FOREIGN KEY (`mpid`) REFERENCES `onepeter_mp`(`id`) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;