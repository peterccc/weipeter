<?php
namespace app\mp\model;

use EasyWeChat\Foundation\Application;
use think\Model;

class MpQrcode extends Model  {
    protected $autoWriteTimestamp = true;
    protected $updateTime = null;


    const TYPE_TMP_QRCODE_ID = 1;
    const TYPE_FOREVER_QRCODE_ID = 2;
    const TYPE_FOREVER_QRCODE_STR = 3;
    /**
     * @param $app Application
     * @param $mpid
     * @param $scene_name
     * @param $keyword
     * @param $expire
     * @return bool
     */
    public static function addTmpQrcode($app,$mpid,$scene_name,$scene_id,$keyword,$expire) {
        $qrcode = new MpQrcode();
        $qrcode['mpid'] = $mpid;
        $qrcode['type'] = self::TYPE_TMP_QRCODE_ID;
        $qrcode['scene_name'] = $scene_name;
        $qrcode['scene_id'] = $scene_id;
        $qrcode['keyword'] = $keyword;

        // 申请临时二维码
        $result = $app->qrcode->temporary($scene_id,$expire);
        $qrcode['ticket'] = $result->ticket;
        $qrcode['expire'] = time() + $result->expire_seconds;
        $qrcode['url'] = $result->url;

        // 短网址
        $qrcode['short_url'] = $app->url->shorten($result->url)['short_url'];

        if($qrcode->save()) {
            return true;
        }else {
            return false;
        }
    }

    /**
     * @param $app Application
     * @param $data
     * @return bool
     */
    private static function addForeverQrcode($app,$data) {
        $qrcode = new MpQrcode();

        // 申请临时二维码
        switch ($data['type']){
            case self::TYPE_FOREVER_QRCODE_ID:
                $result = $app->qrcode->forever($data['scene_id']);
                break;
            case self::TYPE_FOREVER_QRCODE_STR:
                $result = $app->qrcode->forever($data['scene_str']);
                break;
        }
        $qrcode['ticket'] = $result->ticket;
        $qrcode['url'] = $result->url;
        $qrcode['short_url'] = $app->url->shorten($result->url)['short_url'];

        if($qrcode->save($data)) {
            return true;
        }else {
            return false;
        }
    }
    public static function addForeverQrcodeId($app,$mpid,$scene_name,$scene_id,$keyword) {
        return self::addForeverQrcode($app,[
            'mpid'=>$mpid,
            'scene_name'=>$scene_name,
            'scene_id'=>$scene_id,
            'keyword'=>$keyword,
            'type'=>self::TYPE_FOREVER_QRCODE_ID
        ]);
    }
    public static function addForeverQrcodeStr($app,$mpid,$scene_name,$scene_str,$keyword) {
        return self::addForeverQrcode($app,[
            'mpid'=>$mpid,
            'scene_name'=>$scene_name,
            'scene_str'=>$scene_str,
            'keyword'=>$keyword,
            'type'=>self::TYPE_FOREVER_QRCODE_STR
        ]);
    }


    /**
     * @var Application
     */
    private static $app;
    public static function setApp($app) {
        self::$app = $app;
    }
    public function getQrcodeAttr($value,$data) {
        return self::$app->qrcode->url($data['ticket']);
    }
    public function getExpireDateAttr($value,$data) {
        if($data['expire']) {
            return date('Y-m-d H:i:s',$data['expire']);
        }else {
            return '无限';
        }
    }
    public function getStateAttr($value,$data) {
        if($data['expire'] >= time() || !$data['expire']) {
            return '未过期';
        }else {
            return '过期';
        }
    }
}