<?php
namespace app\mp\model;

use EasyWeChat\Foundation\Application;
use think\Model;

class MpQrcodeStatistics extends Model {
    protected $autoWriteTimestamp = true;
    protected $updateTime = null;

    public static function addStatistics($mpid,$openid,$qrcode) {
        $statistics = new MpQrcodeStatistics();
        $statistics['mpid'] = $mpid;
        $statistics['openid'] = $openid;
        $statistics['scene_name'] = $qrcode['scene_name'];
        $statistics['keyword'] = $qrcode['keyword'];
        $statistics['scene_type'] = $qrcode['type'];
        $statistics['scene_id'] = $qrcode['scene_id'];
        $statistics['scene_str'] = $qrcode['scene_str'];
        if($statistics->save()){
            return true;
        }else {
            return false;
        }
    }

    /**
     * @var Application
     */
    private static $app;
    public static function setApp($app) {
        self::$app = $app;
    }
    public function getHeadimgurlAttr($value,$data){
        return self::$app->user->get($data['openid'])->headimgurl;
    }
    public function getNicknameAttr($value,$data) {
        return self::$app->user->get($data['openid'])->nickname;
    }
}