<?php
namespace app\mp\model;
use EasyWeChat\Foundation\Application;
use think\Model;

class MpMessage extends Model {
    const TYPE_TEXT = 1;

    /**
     * @var Application
     */
    private static $app;


    public static function addText($mpid,$openid,$text) {
        $msg = new MpMessage();
        $msg['mpid'] = $mpid;
        $msg['openid'] = $openid;
        $msg['type'] = self::TYPE_TEXT;
        $msg['content'] = $text;
        $msg['create_time'] = time();
        if($msg->save()) {
            return true;
        }else {
            return false;
        }
    }


    public static function setApp($app) {
        self::$app = $app;
    }
    public function getHeadimgurlAttr($value,$data) {
        return self::$app->user->get($data['openid'])['headimgurl'];
    }
    public function getNicknameAttr($value,$data) {
        return self::$app->user->get($data['openid'])['nickname'];
    }
    public function getTypeAttr($value,$data) {
        $typeList = [self::TYPE_TEXT=>'文本'];
        return $typeList[$value];
    }
    public function getContentAttr($value,$data) {
        if($data['type'] == self::TYPE_TEXT) {
            return $data['content'];
        }else {
            return '';
        }
    }
}