<?php
    return [
        'nav_side_bar_config' => [
            [
                'title' => '标题1',
                'buttonList' => [
                    [
                        'auth' => true,
                        'title' => '需要授权的页面',
                        'href' => url('index/admin/index'),
                    ],
                    [
                        'title' => '按钮1',
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮2',
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮3',
                        'href' => '#',
                    ],
                ]
            ],
            [
                'title' => '标题2',
                'buttonList' => [
                    [
                        'title' => '按钮1',
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮2',
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮3',
                        'href' => '#',
                    ],
                ]
            ],
        ]
    ];