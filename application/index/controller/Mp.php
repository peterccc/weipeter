<?php
namespace app\index\controller;

use app\admin\controller\MpController;

class Mp extends MpController {


    public function messageHandler($message)
    {
        $context = $this->getContext();
        if(!$context->inContext()) {
            $data['step'] = 1;
            $context->beginContext(300,$data);
            return "公众号: 你好，请告知你要查询的快递服务商,例如:顺丰";
        }else {
            $data = $context->getData();
            switch ($data['step']) {
                default:
                case 1:
                    $context->keepContext(300,['step'=>2]);
                    return "你好，请输入你要查询的{$message->Content}快递单号";
                    break;
                case 2:
                    $context->endContext();
                    return "单号:{$message->Content},以下是你要查询的内容:xxxxx";
                    break;
            }


        }
    }
}