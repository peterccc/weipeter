<?php
namespace app\index\controller;

use app\admin\controller\MpOauth;

class Index extends MpOauth
{
    public function index()
    {
        dump($this->getInfo());
    }

    /**
     * 全局授权
     * 返回需要的Scopes，如果这里返回的不是SCOPES_NOT_OAUTH，则所有方法都会被网页授权，如果返回SCOPES_NOT_OAUTH，则所有网页都不会被网页授权
     * @return mixed
     */
    protected function getScopes()
    {
        return [self::SCOPES_NOT_OAUTH];
    }
}
