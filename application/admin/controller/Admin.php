<?php

namespace app\admin\controller;

use app\admin\core\Module;
use app\admin\model\RoleAuth;
use app\admin\model\RoleMenuReadable;
use app\common\constant\Mysql;
use app\common\constant\CmsUser;
use app\common\session\CmsUserSession as UserSession;
use AppserverIo\Lang\Reflection\ReflectionClass;
use AppserverIo\Lang\Reflection\ReflectionException;
use think\Controller;
use think\Request;

abstract class Admin extends Controller {
    protected function _initialize()
    {
        parent::_initialize();
        // 用户信息
        $user = UserSession::getUser();
        $this->assign('_user', $user);
        // 验证是否登录
        if(!$user){
            $this->redirect('admin/Access/login');
        }

        // 验证是否足够权限
        if ($user['role_id'] != CmsUser::ROLE_ID_ADMIN) {
            $request = Request::instance();
            $moduleName = $request->module();
            $methodName = $request->action();

            // 是否需要验证
            $class = "\\app\\{$moduleName}\\controller\\Admin";

            try {
                (new ReflectionClass($class))
                    ->getMethod($methodName)
                    ->getAnnotation('Auth');
                $actionId = actionId($moduleName, $methodName);
                if (! RoleAuth::get(['role_id' => $user['role_id'], 'action_id' => $actionId]) ) {
                    $this->error("拒绝访问！");
                }
            }catch (ReflectionException $e) {
            }


        }


        // 生成页面
        $install = new Module();


        // 生成导航条
        /*$_navToolBarConfig = [
            [
                'title' => '首页',
                'active' => true,
                'href' => '#'
            ],
            [
                'title' => '用户',
                'href' => '#',
            ],
        ];*/
        $this->assign('advancedManager', $user['advanced_manager']);
        if ($user['advanced_manager'] === Mysql::TRUE) {
            if($config = $install->getConfig("admin")){
                $_navToolBarConfig = $config['nav_tool_bar_config'];
                $this->assign('_navToolBarConfig',$_navToolBarConfig);
            }
        }


        // 生成侧边导航栏
        /*$_navSideBarConfig = [
            [
                'title' => '标题1',
                'active' => true,
                'buttonList' => [
                    [
                        'title' => '按钮1',
                        'active' => true,
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮2',
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮3',
                        'href' => '#',
                    ],
                ]
            ],
            [
                'title' => '标题2',
                'buttonList' => [
                    [
                        'title' => '按钮1',
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮2',
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮3',
                        'href' => '#',
                    ],
                ]
            ],
        ];*/
        $list = $install->getInstalledModuleList();
        $_navSideBarConfig = [];
        foreach ($list as $module) {
            $config = $install->getConfig($module['name']);
            $config['nav_side_bar_config'] = $this->authNavConfig($module['name'], $config['nav_side_bar_config']);
            $_navSideBarConfig = array_merge($_navSideBarConfig,$config['nav_side_bar_config']);
        }
        $this->assign('_navSideBarConfig',$_navSideBarConfig);

        // 当前公众号
        $curMp = get_selected_mp();
        if($curMp)
            $curMpAlias = $curMp['alias'];
        else
            $curMpAlias = '无';
        $this->assign('curMpAlias',$curMpAlias);

        // 绑定公众号
        $bindMp = get_bind_mp();
        if($bindMp)
            $bindMpAlias = $bindMp['alias'];
        else
            $bindMpAlias = '无';
        $this->assign('bindMpAlias',$bindMpAlias);



    }


    public function success($msg = '', $url = null, $data = '', $wait = 3, array $header = [])
    {
        if($msg == '')
            $msg = "操作成功！";

        parent::success($msg, $url, $data, $wait, $header);
    }

    protected function error($msg = '', $url = null, $data = '', $wait = 3, array $header = [])
    {
        if($msg == '')
            $msg = '操作失败！';
        parent::error($msg, $url, $data, $wait, $header);
    }

    private function authNavConfig($moduleName, $config) {
        $roleId = UserSession::getUser()['role_id'];
        if ($roleId == CmsUser::ROLE_ID_ADMIN) {
            return $config;
        }

        $returnConfig = [];
        foreach ($config as $item) {
            $groupName = $item['title'];
            $returnItem = $item;
            $returnItem['buttonList'] = [];
            foreach ($item['buttonList'] as $button) {
                $buttonName = $button['title'];
                $menuId = menuId($moduleName, $groupName, $buttonName);
                if (isset($button['auth']) && $button['auth'] == true) {
                    if (RoleMenuReadable::get(['menu_id' => $menuId, 'role_id' => $roleId])) {
                        $returnItem['buttonList'][] = $button;
                    }
                }else {
                    $returnItem['buttonList'][] = $button;
                }

            }
            if (!empty($returnItem['buttonList']))
                $returnConfig[] = $returnItem;
        }
        return $returnConfig;
    }
}