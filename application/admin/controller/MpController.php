<?php
namespace app\admin\controller;

use EasyWeChat\Foundation\Application;
use Symfony\Bridge\PsrHttpMessage\Tests\Fixtures\Message;

/**
 * Class MpController
 * @package app\admin\controller
 * 要获取收到信息的公众号则使用Message->ToUserName
 */
abstract class MpController {
    /**
     * @var \app\admin\core\Context
     */
    private $_context;
    private $_app;
    private $_mp;
    public function __construct($context = null,$app = null,$mp = null){
        $this->_context = $context;
        $this->_app = $app;
        $this->_mp = $mp;
    }


    /**
     * 返回接收信息的这个公众号
     * @return null
     */
    public function getMp() {
        return $this->_mp;
    }

    public function getMpId() {
        return $this->_mp['id'];
    }

    /**
     * @return Application
     */
    public function getApp() {
        return $this->_app;
    }
    /**
     * @return \app\admin\core\Context
     */
    public function getContext() {
        return $this->_context;
    }

    /**
     * 关键词 模块消息处理
     * @param $message \Symfony\Bridge\PsrHttpMessage\Tests\Fixtures\Message
     */
    public function messageHandler($message) {}

    /**
     * 返回非false数据，则返回此数据的消息
     * 返回false，则继续执行监听器或自动回复
     * 可以使用context，如果使用context要立即返回消息，否则有可能被其他模块覆盖
     * @param $message Message
     */
    public function messageListener(&$message) {}

    /**
     * 默认消息处理，当消息没有被关键词回复处理，则会被调用
     * 返回非false数据，则返回此数据的消息
     * 返回false，则继续执行监听器或自动回复
     * 可以使用context，如果使用context要立即返回消息，否则有可能被其他模块覆盖
     * @param $message
     */
    public function defaultMessageHandler($message){}
}