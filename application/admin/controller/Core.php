<?php

namespace app\admin\controller;
use app\admin\model\File;
use app\admin\model\Img;


class Core extends Admin
{
    public function uploadImg() {
        $imgTypes = ['jpg','jpeg','gif','png'];
        $idsStr = '';
        $ids = [];
        foreach ($_FILES as $file) {
            $fileParts = pathinfo($file['name']);
            if(!in_array($fileParts['extension'],$imgTypes)) {
                $this->error('只能上传图片');
            }
            $rootPath = ROOT_PATH . DS . 'public';
            $relativePath = 'static'.DS.'uploads'.DS.'images'.DS.date("Y-m-d");
            $filename = time() . rand() . '.' . $fileParts['extension'];
            $targetPath = $rootPath.DS.$relativePath;
            $targetFile = $targetPath.DS.$filename;  // 上传到这个文件名
            $tempFile = $file['tmp_name'];
            $urlPath = str_replace(DS,'/',DS.$relativePath.DS.$filename);


            mkDirs($targetPath);
            if(move_uploaded_file($tempFile,$targetFile)){
                $img = new Img();
                $id = $img->addImg(Img::TYPE_LOCAL,$urlPath);
                $ids[] = ['id'=>$id,'url'=>get_img_url($id)];
                $idsStr.=','.$id;
            }else {
                $this->error("上传失败");
            }
        }
        $idsStr = substr($idsStr,1);
        return ['idsStr'=>$idsStr,'ids'=>$ids];
    }
    public function uploadFile() {
        $FileNotTypes = ['sql','php','js','html','css'];
        $ids = [];
        $idsStr = '';
        foreach ($_FILES as $file) {
            $fileParts = pathinfo($file['name']);
            if(in_array($fileParts['extension'],$FileNotTypes)) {
                $this->error('非法文件');
            }
            $rootPath = ROOT_PATH . DS . 'public';
            $relativePath = 'static'.DS.'uploads'.DS.'images'.DS.date("Y-m-d");
            $filename = time() . rand() . '.' . $fileParts['extension'];

            $targetPath = $rootPath.DS.$relativePath;
            $targetFile = $targetPath.DS.$filename;  // 上传到这个文件名
            $tempFile = $file['tmp_name'];
            $urlPath = str_replace(DS,'/',DS.$relativePath.DS.$filename);


            mkDirs($targetPath);
            if(move_uploaded_file($tempFile,$targetFile)){
                $file = new File();
                $id = $file->addFile(File::TYPE_LOCAL,$urlPath);
                $ids[] = ['id'=>$id,'url'=>get_file_path($id)];
                $idsStr .= ','.$id;
            }else {
                $this->error("上传失败");
            }
        }
        $idsStr = substr($idsStr,1);
        return ['idsStr'=>$idsStr,'ids'=>$ids];
    }
    /*
    public function uploadImg() {
        // Define a destination
        // $targetFolder = "..". DIRECTORY_SEPARATOR ."..". DIRECTORY_SEPARATOR ."uploads" . DIRECTORY_SEPARATOR . "images" .DIRECTORY_SEPARATOR.date("Y-m-d").DIRECTORY_SEPARATOR ; // Relative to the root
        $relativePath = "public" .DIRECTORY_SEPARATOR . "static" . DIRECTORY_SEPARATOR . "uploads". DIRECTORY_SEPARATOR . "images" .DIRECTORY_SEPARATOR.date("Y-m-d").DIRECTORY_SEPARATOR ;
        $urlPath = '/' . str_replace(DIRECTORY_SEPARATOR,'/',$relativePath);
        $targetFolder = ROOT_PATH . $relativePath;


        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath = $targetFolder;
            mkDirs($targetPath);
            $targetPath = realpath($targetPath);
            $fileParts = pathinfo($_FILES['file']['name']);

            $filename = time() . rand() . '.' . $fileParts['extension'];
            $targetFile = rtrim($targetPath,DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $filename;

            // Validate the file type
            $fileTypes = array('jpg','jpeg','gif','png'); // File extensions

            $response = array ();
            if (in_array($fileParts['extension'],$fileTypes)) {
                if(move_uploaded_file($tempFile,$targetFile)){
                    $img = new Img();
                    $id = $img->addImg(Img::TYPE_LOCAL,$urlPath . $filename);
                    $response['success'] = 1;
                    $response['filename'] = $filename;
                    $response['img_id'] = $id;
                    foreach ($_POST as $key => $value){
                        $response[$key] = $value;
                    }
                }else {
                    $response['success'] = 0;
                    $response['error'] = 'Unknown error';
                }
            } else {
                $response['success'] = 0;
                $response['error'] = 'Invalid file type.';
            }
            return $response;
        }
    }*/
}