<?php
namespace app\admin\controller;

vendor('wechat/autoload');
use EasyWeChat\Foundation\Application;
use think\Config;
use think\Url;

/**
 * Class MpAdmin
 * @package app\admin\controller
 * 要获取后台当前选择的公众号则使用get_selected_mp()
 */
abstract class MpAdmin extends Admin {
    /**
     * @var Application
     */
    private $app;

    /**
     * @return Application
     */
    protected function getApp() {
        return $this->app;
    }

    /**
     * 获取后台当前选择的公众号
     * @return mixed
     */
    protected function getMp() {
        return get_selected_mp();
    }

    protected function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub
        $selectedMp = get_selected_mp();
        if($selectedMp === null) {
            $this->error("请先选择微信公众号",Url::build('admin/Mp/mpList'));
        }

        $options = array_merge(
            Config::get('mp_options'),
            [
                'app_id'=> $selectedMp['appid'],
                'secret'=> $selectedMp['appsecret'],
                'token' => $selectedMp['token'],
            ]
        );
        $this->app = new Application($options);
    }
}