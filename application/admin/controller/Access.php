<?php
namespace app\admin\controller;
use app\admin\model\User;
use app\common\constant\Status;
use think\Controller;
use think\Request;
use think\Url;
use app\common\session\CmsUserSession as UserSession;
class Access extends Controller{
    public function login(Request $request,$username='',$password='',$captcha='') {
        if($request->isGet()) {
            // 登录页面
            return $this->fetch();
        }else {
            // 登录处理
            if(!captcha_check($captcha)){
                $this->error("验证码错误",url('login'));
            }
            $salt = config("salt");
            $user = User::get(['username' => $username, 'password'=>md5($salt.$password)]);

            if(!$user || $user['status'] != Status::ON) {
                $this->error("用户名或密码错误", url('login'));
            }

            UserSession::setUser($user);
            $this->redirect(Url::build('Index/index'));
        }
    }
    public function logout() {
        UserSession::setUser(null);
        $this->redirect(Url::build('login'));
    }
}