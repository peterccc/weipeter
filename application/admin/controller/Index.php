<?php

namespace app\admin\controller;

use app\admin\builder\AdminBuilder;
use app\admin\builder\ConfigBuilder;
use app\admin\builder\ListBuilder;
use app\admin\core\Module as ModuleInstall;
use app\common\constant\CmsUser;
use app\common\session\CmsUserSession;
use think\Request;
use think\Url;

class Index extends Admin
{
//    测试代码
    /*public function configBuilder2() {
        $builder = new ConfigBuilder();
        $data = $builder->handleConfig();
        return $builder
            ->title("handleConfig测试")
            ->keyMultiImage('head',"图片","上传图片",3)
            ->keyMultiImage('head2',"图片","上传图片",3)
            ->keyText(null,"输入框1","子标题")
            ->keyText("text2","输入框2")
            ->keyEditor('rich1',"富文本编辑框",true)
            ->keyEditor('rich2',"富文本编辑框")
            ->keyTextArea("text3","TextArea","子标题")
            ->data($data)
            ->buttonSubmit()
            ->fetch();
    }*/
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    /*public function configBuilder()
    {
        $data = [
            'text1' => '文本1',
            'text2' => '文本2',
            'text3' => '文本域',
            'rech'  => '富文本内容',
            'head'  => '37',
            'head2' => '37'
        ];
        $builder = new ConfigBuilder();
        return $builder
            ->title("测试标题")
            ->keyMultiImage('head',"图片","上传图片",3)
            ->keyMultiImage('head2',"图片","上传图片",3)
            ->keyText("text1","输入框1","子标题")
            ->keyText("text2","输入框2")
            ->keyEditor('rech',"富文本编辑框",true)
            ->keyTextArea("text3","TextArea","子标题")
            ->buttonSubmit(Url::build("config"))
            ->buttonBack()
            ->data($data)
            ->fetch();
    }*/

    /*public function listBuilder(Request $request) {
        $data = [
            ['id'=>'6','img'=>'12,13,14','link'=>1,'text1' => '文本1','text2' => '文本2','text3' => '文本3','text4' => '文本4'],
            ['id'=>'2','img'=>'12,13,14','link'=>1,'text1' => '文本1','text2' => '文本2','text3' => '文本3','text4' => '文本4'],
            ['id'=>'3','img'=>'12,13,14','link'=>1,'text1' => '文本1','text2' => '文本2','text3' => '文本3','text4' => '文本4'],
            ['id'=>'4','img'=>'12,13,14','link'=>1,'text1' => '文本1','text2' => '文本2','text3' => '文本3','text4' => '文本4'],
        ];
        $builder = new ListBuilder();
        return $builder
            ->setSelectUrl(Url::build($request->action()))
            ->select("分类","cate",[0=>'全部',1=>'大',2=>'中',3=>'小','other'=>'其他'],"分类产品")
            ->select("分类2","cate2",[0=>'全部2',1=>'大2',2=>'中2',3=>'小2','other'=>'其他2'],"分类产品2")
            ->setSearchUrl(Url::build($request->action()))
            ->search()
            ->search()
            ->keyLink("测试操作","http://www.baidu.com/###")
            ->title("列表测试标题")
            ->suggest("测试子标题")
            ->keyDoAction(urldecode(Url::build('config',['id'=>'###'])),'编辑',false)
            ->keyDoAction(urldecode(Url::build('config',['id'=>'###'])),'删除')
            ->keyDoAction(urldecode(Url::build('config',['id'=>'###'])),'删除')
            ->pagination(20,5)
            ->keyImage('img','图片')
            ->buttonNew('')
            ->buttonDelete(url('config'))
            ->buttonBack()
            ->keyText("text1","标题1")
            ->keyText("text2","标题2")
            ->keyText("text2","标题2")
            ->keyText("text2","标题2")
            ->keyText("text2","标题2")
            ->keyText("text2","标题2")
            ->keyText("text3","标题3")
            ->keyText("text4","标题4")
            ->data($data)
            ->fetch();
    }*/

    /*public function config(Request $request) {
        if($request->isPost()) {
            dump($_POST);
        }
    }


    public function ajaxTest() {
        return $this->fetch();
    }
    public function ajaxPost($name,$age) {
        $this->success("success");
    }
    public function upload() {
        return $this->fetch();
    }
    public function ajaxUpload(Request $request) {
        dump($_FILES);
    }*/

//    以上都是测试代码


    private function authAdvancedManager() {
        $user = CmsUserSession::getUser();
        if ($user['role_id'] !== CmsUser::ROLE_ID_ADMIN) {
            if($user['advanced_manager'] == false) {
                $this->error("拒绝访问");
            }
        }
    }


    public function index() {
        $install = new ModuleInstall();
        $config = $install->getConfig('admin');
        foreach ($config['nav_tool_bar_config'] as $module) {
            if(isset($module['home_page']) && $module['home_page']) {
                $this->redirect($module['href']);
            }
        }
        return $this->fetch();
    }

    public function module(){
        $this->authAdvancedManager();
        $install = new ModuleInstall();
        $data = $install->getModuleList();
        foreach ($data as &$module) {
            if($module['status']['installed']) {
                $module['status'] = true;
            }else {
                $module['status'] = false;
            }
        }

        $builder = new ListBuilder();
        return $builder
            ->title("模块管理")
            ->keyText('name',"模块名")
            ->keyText('status','状态',[true=>'已安装',false=>'未安装'])
            ->keyDoAction(function ($value){
                if($value['status']) {
                    return url('admin/Index/moduleUninstall',['moduleName'=>$value['name']]);
                }else {
                    return url('admin/Index/moduleInstall',['moduleName'=>$value['name']]);
                }
            },function($value){
                if($value['status']) {
                    return "卸载";
                }else {
                    return "安装";
                }
            })
            ->keyDoAction(urldecode(Url::build('moduleClearData',['moduleName'=>'###'])),'清理数据',true,'操作','name')
            ->data($data)
            ->fetch();
    }

    public function moduleInstall($moduleName) {
        $this->authAdvancedManager();
        $install = new ModuleInstall();
        if($install->install($moduleName)) {
            $this->success("安装成功");
        }else {
            $this->error("请先卸载");
        }
    }
    public function moduleUninstall($moduleName) {
        $this->authAdvancedManager();
        $install = new ModuleInstall();
        if($install->uninstall($moduleName)) {
            $this->success("卸载成功");
        }else {
            $this->error("请先安装");
        }
    }
    public function moduleClearData($moduleName) {
        $this->authAdvancedManager();
        $install = new ModuleInstall();
        if($install->clearData($moduleName)) {
            $this->success("清空数据成功");
        }else {
            $this->error("请先安装");
        }
    }

//    public function test() {
//        $builder = new ConfigBuilder();
//        $data = $builder->handleConfig();
//        return $builder
//            ->title("测试")
//            ->keyMultiImage("TEST_IMG", '测试图片')
//            ->keyMultiFile("TEST_FILE","测试文件")
//            ->keySingleImage("TEST_SINGLE_IMG", '测试单图片上传')
//            ->keySingleFile("TEST_SINGLE_FILE", "测试单文件上传")
//            ->keyEditor("TEST_Editor", "测试编辑器")
//            ->data($data)
//            ->buttonSubmit()
//            ->fetch();
//    }
}
