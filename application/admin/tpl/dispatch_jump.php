{__NOLAYOUT__}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>跳转提示</title>
    <link rel="stylesheet" href="__STATIC__/lib/bootstrap/css/bootstrap.css">
    <style type="text/css">
    </style>
</head>
<body>
    <div class="container">
        <div class="row col-xs-12">
            <h2>
                <?php
                    switch ($code) {
                    case 1:
                        echo "成功";
                        break;
                    case 0:
                        echo "失败";
                        break;
                    }
                ?>
            </h2>
        </div>
        <div class="row col-xs-12">
            <p class="lead"><?=strip_tags($msg)?></p>
        </div>
        <div class="row col-xs-12" >
            <p class="lead">
                页面还有 <span id="wait"><?=$wait?></span> 秒后 <a href="<?=$url?>">跳转</a>
            </p>
        </div>
    </div>
    <script type="text/javascript">
        <?php // 成功与否，1成功，0失败：  $code?>
        <?php // 信息: echo(strip_tags($msg));?>
        <?php // 页面跳转url：php echo($url); ?>
        <?php // 页面等待秒数：echo($wait);?>
        (function(){
            var wait = document.getElementById('wait'),
                // href = document.getElementById('href').href;
                href = '<?=$url?>';
            var interval = setInterval(function(){
                var time = --wait.innerHTML;
                if(time <= 0) {
                    location.href = href;
                    clearInterval(interval);
                };
            }, 1000);
        })();
    </script>
</body>
</html>
