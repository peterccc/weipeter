<?php

namespace app\admin\builder;


use app\common\model\Config;
use think\Url;

class ConfigBuilder extends AdminBuilder
{
    private $_postUrl = '';
    private $_data = [];
    /**
     * @var array $_keyList
     * [
     *   'name' => $name,
     *   'title'=> $title,
     *   'subtitle'=>$subtitle,
     *   'type' => $type,
     *   'opt'  => $opt
     *   ]
     */
    private $_keyList = [];
    PRIVATE $_buttonList = [];


    public function fetch($template = '', $vars = [], $replace = [], $config = [])
    {
        // 给予空的data
        foreach ($this->_keyList as $item) {
            if(!isset($this->_data[$item['name']])){
                $this->_data[$item['name']] = '';
            }
        }
        // 编译按钮的html属性
        foreach ($this->_buttonList as &$button) {
            $button['attr'] = $this->compileHtmlAttr($button['attr']);
        }

        // 解析照片
//        foreach ($this->_keyList as $key) {
//            if($key['type'] == "multiImage") {
//                $ids = $this->_data[$key['name']];
//                $ids = explode(",",$ids);
//                $this->_data[$key['name']] = $ids;
//            }
//        }


        $this->assign("_data",$this->_data);
        $this->assign("_postUrl",$this->_postUrl);
        $this->assign("_keyList",$this->_keyList);
        $this->assign("_buttonList",$this->_buttonList);
        return parent::fetch("config_builder", $vars, $replace, $config);
    }



    public function setPostUrl($url) {
        $this->_postUrl = $url;
        return $this;
    }

    public function data($data) {
        $this->_data = $data;
        return $this;
    }

    public function key($name, $title, $subtitle, $type, $opt = null) {
        $key = [
            'name' => $name,
            'title'=> $title,
            'subtitle'=>$subtitle,
            'type' => $type,
            'opt'  => $opt
        ];
        $this->_keyList[] = $key;
        return $this;
    }

    public function keyText($name, $title, $subtitle = null) {
        return $this->key($name,$title,$subtitle,"text");
    }

    public function keyPassword($name, $title, $subtitle = null) {
        return $this->key($name, $title, $subtitle, "password");
    }

    public function keyReadOnly($name,$title,$subtitle = null) {
        return $this->key($name,$title,$subtitle,"readonly");
    }

    public function keyTextArea($name,$title,$subtitle = null) {
        return $this->key($name,$title,$subtitle,"textarea");
    }

    /**
     * @param $name
     * @param $title
     * @param $allToolbars bool 是否列出所有工具栏按钮
     * @param null $subtitle
     * @return ConfigBuilder
     */
    public function keyEditor($name,$title,$allToolbars = false,$subtitle = null) {
        return $this->key($name,$title,$subtitle,"editor",['all_toolbars'=>$allToolbars]);
    }

    // 有个bug，退后或前进会重新上传一次文件
    /**
     * @param $name string       name对应的值例如: "1,2,3"
     * @param $title
     * @param null $subtitle
     * @param string $limit
     * @return ConfigBuilder
     */
    public function keyMultiImage($name,$title,$subtitle = null) {
        return $this->key($name,$title,$subtitle,"multiImage");
    }

    public function keySingleImage($name,$title,$subtitle = null) {
        return $this->key($name,$title,$subtitle,"singleImage");
    }

    public function keyMultiFile($name, $title, $subtitle = null) {
        return $this->key($name,$title,$subtitle,"multiFile");
    }

    public function keySingleFile($name, $title, $subtitle = null) {
        return $this->key($name,$title,$subtitle,"singleFile");
    }

    /**
     * @param $name
     * @param $title
     * @param $options [1=>'男',2=>'女']
     * @param null $subtitle
     * @return ConfigBuilder
     */
    public function keySelect($name,$title,$options,$subtitle = null) {
        return $this->key($name,$title,$subtitle,'select',['options'=>$options]);
    }

    public function button($title,$attr=array()) {
        $this->_buttonList[] = [
            "title" => $title,
            'attr'  => $attr,
        ];
        return $this;
    }
    public function buttonSubmit($url = '' , $title="确定") {
        // 设置postUrl
        if($url == '' ){
            $url = Url::build($this->_request->action(),$this->_request->param());
        }
        $this->setPostUrl($url);

        $attr = array();
        $attr['class'] = "btn btn-success ajax-post-submit";
        $attr['type'] = 'submit';
        return $this->button($title, $attr);
    }

    public function buttonBack($title = '返回')
    {
        $attr = array();
        $attr['onclick'] = 'javascript:history.back(-1);return false;';
        $attr['class'] = 'btn btn-default';
        $attr['type'] = 'button';
        return $this->button($title, $attr);
    }

    public function buttonJump($title, $href) {
        $attr = array();
        $attr['onclick'] = "javascript:window.location.href='{$href}';";
        $attr['class'] = 'btn btn-default';
        $attr['type'] = 'button';
        return $this->button($title, $attr);
    }

    public function handleConfig() {
        if($this->_request->isPost()) {
            $post = $this->_request->post();
//            dump($post);
//            exit();
            foreach ($post as $name=>$value) {
                Config::setConfig($name,$value);
            }
            $this->success("设置成功");
        }else {
            $all = Config::getAll();
            $data = [];
            foreach ($all as $item) {
                $data[$item['name']] = $item['value'];
            }
            return $data;
        }
    }
}
