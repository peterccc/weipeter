<?php
return [
    'nav_tool_bar_config' => [
        [
            'title' => '模块管理',
            'href' => \think\Url::build('admin/Index/module'),
        ],
        [
            'title' => '公众号管理',
            'href' => \think\Url::build('admin/Mp/mpList'),
            //'home_page' => true,
        ],
        [
            'title' => '回复管理',
            'href' => \think\Url::build('admin/Mp/mpReplyList'),
        ],
//        [
//            'title' => '测试',
//            'href' => \think\Url::build('admin/Index/test'),
//        ],
    ],
];