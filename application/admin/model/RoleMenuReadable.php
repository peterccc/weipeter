<?php
namespace app\admin\model;
use think\Model;
class RoleMenuReadable extends Model {
    protected $name = 'cms_role_menu_readable';
    protected $autoWriteTimestamp = false;
    // protected $autoWriteTimestamp = true;
    // protected $createTime = 'create_at';
//    protected $updateTime = false;
}