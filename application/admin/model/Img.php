<?php
namespace app\admin\model;
use think\Model;
class Img extends Model {
    protected $name = 'cms_img';

    const TYPE_LOCAL = 1;
    const TYPE_EXTERNAL = 2;

    /**
     * 添加图片到数据库
     * @param $type string  图片路径类型
     * @param $path string  图片路径
     * @return bool|int     成功返回图片id，失败返回false
     */
    public function addImg($type,$path) {
        $data['type'] = $type;
        $data['path'] = $path;
        if($result = $this::create($data)) {
            return $result['id'];
        }else {
            return false;
        }
    }

    /**
     * 通过图片id获取图片
     * @param $id       图片id
     * @return static   返回图片数据模型
     */
    public function getImg($id) {
        $img = $this::get($id);
        return $img;
    }
}