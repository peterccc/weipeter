<?php
namespace app\admin\model;
use think\Model;

class MpReply extends Model {
    const TYPE_TEXT = 1;
    const TYPE_MODULE = 2;
    const TYPE_IMAGE = 3;
    const TYPE_NEWS = 4;
    const TYPE_MESSAGE_LISTENER = 5;
    const TYPE_DEFAULT_MESSAGE_HANDLER = 6;

    /**
     * 默认消息处理，当消息没有被关键词回复处理，则会被调用
     * @param $moduleName
     * @return bool
     */
    public static function addDefaultMessageHandler($moduleName) {
        $reply = new MpReply();
        $reply['content'] = $moduleName;
        $reply['type'] = self::TYPE_DEFAULT_MESSAGE_HANDLER;
        if($reply->save()) {
            return true;
        }else {
            return false;
        }
    }
    /**
     * 添加消息监听
     * @param $moduleName
     * @return bool
     */
    public static function addMessageListener($moduleName) {
        $reply = new MpReply();
        $reply['content'] = $moduleName;
        $reply['type'] = self::TYPE_MESSAGE_LISTENER;
        if($reply->save()) {
            return true;
        }else {
            return false;
        }
    }
    public static function addReplyNews($keyword,$json) {
        $reply = new MpReply();
        $reply['keyword'] = $keyword;
        $reply['content'] = $json;
        $reply['type'] = self::TYPE_NEWS;
        if($reply->save()) {
            return true;
        }else {
            return false;
        }
    }
    public static function addReplyImage($keyword, $image) {
        $reply = new MpReply();
        $reply['keyword'] = $keyword;
        $reply['content'] = $image;
        $reply['type'] = self::TYPE_IMAGE;
        if($reply->save()) {
            return true;
        }else {
            return false;
        }
    }
    public static function addReplyText($keyword,$text) {
        $reply = new MpReply();
        $reply['keyword'] = $keyword;
        $reply['content'] = $text;
        $reply['type'] = self::TYPE_TEXT;
        if($reply->save()){
            return true;
        }else {
            return false;
        }
    }
    public static function addReplyModule($keyword,$moduleName) {
        $reply = new MpReply();
        $reply['keyword'] = $keyword;
        $reply['content'] = $moduleName;
        $reply['type'] = self::TYPE_MODULE;
        if($reply->save()){
            return true;
        }else {
            return false;
        }

    }
    public static function getReplyText($keyword) {
        if($reply = MpReply::get(['keyword'=>$keyword,'type'=>self::TYPE_TEXT])){
            return $reply['content'];
        }else {
            return null;
        }
    }
    public static function getReplyModule($keyword) {
        if($reply = MpReply::get(['keyword'=>$keyword,'type'=>self::TYPE_MODULE])){
            return $reply['content'];
        }else {
            return null;
        }
    }

    public function getKeywordAttr($value,$data) {
        if($data['type'] == self::TYPE_DEFAULT_MESSAGE_HANDLER || $data['type']==self::TYPE_MESSAGE_LISTENER) {
            return '模块名: '.$data['content'];
        }else {
            return $data['keyword'];
        }
    }
}