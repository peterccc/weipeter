<?php
namespace app\admin\model;
use think\Model;
class File extends Model {
    protected $name = 'cms_file';

    const TYPE_LOCAL = 1;
    const TYPE_EXTERNAL = 2;

    public function addFile($type,$path) {
        $data['type'] = $type;
        $data['path'] = $path;
        if($result = $this::create($data)) {
            return $result['id'];
        }else {
            return false;
        }
    }

    public function getFile($id) {
        $file = $this::get($id);
        return $file;
    }
}