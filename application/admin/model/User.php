<?php
namespace app\admin\model;
use think\Model;
class User extends Model {
    protected $name = 'cms_user';
    protected $autoWriteTimestamp = true;
    // protected $autoWriteTimestamp = true;
    // protected $createTime = 'create_at';
    protected $updateTime = false;



    public function setPasswordAttr($value, $data) {
        $salt = config("salt");
        return md5($salt.$value);
    }

    public function getRoleNameAttr($value, $data) {
        return $this['role']['name'];
    }

    public function role() {
        return $this->belongsTo('Role');
    }

}