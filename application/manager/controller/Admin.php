<?php
namespace app\manager\controller;


use app\admin\builder\ConfigBuilder;
use app\admin\builder\ListBuilder;
use app\admin\core\Module;
use app\admin\model\Role;
use app\admin\model\RoleAuth;
use app\admin\model\RoleMenuReadable;
use app\admin\model\User;
use app\common\constant\Mysql;
use AppserverIo\Lang\Reflection\ReflectionAnnotation;
use AppserverIo\Lang\Reflection\ReflectionClass;
use AppserverIo\Lang\Reflection\ReflectionException;
use think\Request;

class Admin extends \app\admin\controller\Admin
{
    /************************************************************/
    /*                       后台管理                          */
    /************************************************************/
    public function backendManager() {
        $builder = new ConfigBuilder();
        $data = $builder->handleConfig();
        return $builder
            ->title("后台管理")
            ->keyText('WEIPETER_TITLE', '标题')
            ->keySingleImage('WEIPETER_LOGO', '图标')
            ->data($data)
            ->buttonSubmit()
            ->fetch();
    }
    /************************************************************/
    /*                       管理员管理                          */
    /************************************************************/
    /**
     * @return mixed
     * @Auth(desc="用户管理")
     */
    public function adminManager() {
        $data = User::all();

        $builder = new ListBuilder();

        return $builder
            ->title("后台用户管理")
            ->setStatusUrl(url("adminDelete"))
            ->buttonNew(url('adminConfig'))
            ->buttonDelete()
            ->keyText("username", "用户名")
            ->keyText("nickname", "昵称")
            ->keyText("desc", "描述")
            ->keyText("role_name", "角色")
            ->keyText("advanced_manager", "高级管理", [Mysql::TRUE=>"启用", Mysql::FALSE=>"禁用"])
            ->keyCreateTime()
            ->keyDoAction(urldecode(url("adminConfig", ['id'=>'###'])), "编辑", false)
            ->data($data)
            ->fetch();
    }

    /**
     * @return mixed
     * @Auth(desc="用户设置")
     */
    public function adminConfig(Request $request, $id = null) {
        if ($request->isPost()) {


            if ($id === null) {
                // 创建
                $user = new User();
            }else {
                $user = User::get($id);
            }

            if ($request->post('password') == '') {
                if ($id === null) {
                    $this->error("密码不能为空");
                }
            }else {
                $user['password'] = $request->post('password');
            }

            $user['username'] = $request->post('username');
            $user['nickname'] = $request->post('nickname');
            $user['desc'] = $request->post('desc');
            $user['role_id'] = $request->post('role_id');
            $user['advanced_manager'] = $request->post('advanced_manager');


            $user->validate(true)->save();
            if (!$user->getError()) {
                $this->success('', url('adminManager'));
            }else {
                $this->error($user->getError());
            }
        }else {
            $builder = new ConfigBuilder();
            $roleList = Role::all();
            $roleSelect = [];
            foreach($roleList as $role) {
                $roleSelect[$role['id']] = $role['name'];
            }
            if ($id === null ) {
                // 创建
                $builder
                    ->title("创建用户");
            }else{
                $user = User::get($id);
                $builder
                    ->title("编辑用户")
                    ->data($user);
            }
            return $builder
                ->keyText("username", "用户名")
                ->keyPassword("password", "密码")
                ->keyText('nickname', "昵称")
                ->keyText("desc", "描述")
                ->keySelect("role_id", "角色", $roleSelect)
                ->keySelect("advanced_manager", "高级管理", [Mysql::TRUE=>'启用',Mysql::FALSE=>'禁用'], "显示高级管理菜单")
                ->buttonSubmit()
                ->buttonBack()
                ->fetch();
        }
    }

    /**
     * @return mixed
     * @Auth(desc="删除用户")
     */
    public function adminDelete($ids, $status) {
        if ($status == -1) {
            if(User::destroy($ids)) {
                $this->success("删除成功！");
            }else {
                $this->error("删除失败！");
            }
        }
    }

    /**
     * 角色管理
     * @Auth(desc="角色管理")
     */
    public function roleManager() {
        $data = Role::all();

        $builder = new ListBuilder();
        return $builder
            ->title("后台角色管理")
            ->setStatusUrl(url("roleDelete"))
            ->buttonNew(url('roleConfig'))
            ->buttonDelete()
            ->keyText("name", "角色名")
            ->keyText("desc", "描述")
            ->keyDoAction(urldecode(url("roleConfig", ['id'=>'###'])), "编辑", false)
            ->data($data)
            ->fetch();
    }

    /**
     * @param Request $request
     * @param null $id
     * @return mixed
     * @Auth(desc="角色配置")
     */
    public function roleConfig(Request $request, $id = null) {
        if ($request->isPost()) {
            if ($id === null) {
                // 创建
                $role = new Role();
            }else {
                $role = Role::get($id);
            }
            $role['name'] = $request->post('name');
            $role['desc'] = $request->post('desc');
            $role->validate(true)->save();
            if(!$role->getError()) {
                $this->success('', url('roleManager'));
            }else {
                $this->error($role->getError());
            }
        }else {
            $builder = new ConfigBuilder();

            if ($id === null) {
                // 创建
                $builder
                    ->title("创建角色");
            }else {
                $role = Role::get($id);
                $builder
                    ->title("编辑角色")
                    ->data($role);
            }
            return $builder
                ->keyText("name", '角色名')
                ->keyText('desc', '描述')
                ->buttonSubmit()
                ->buttonBack()
                ->fetch();
        }
    }

    /**
     * @param $ids
     * @param $status
     * @Auth(desc="删除角色")
     */
    public function roleDelete($ids, $status) {
        if ($status == -1) {
            if(Role::destroy($ids)) {
                $this->success("删除成功！");
            }else {
                $this->error("删除失败！");
            }
        }
    }


    /**
     * 菜单可见管理
     */
    /**
     * @return mixed
     * @Auth(desc="菜单管理")
     */
    public function menuManager() {
        $moduleManager = new Module();
        $data = [];
        foreach($moduleManager->getInstalledModuleList() as $moduleName => $module) {
            $config = $moduleManager->getConfig($moduleName);
            foreach ($config['nav_side_bar_config'] as $navGroup) {
                $groupTitle = $navGroup['title'];
                foreach ($navGroup['buttonList'] as $navButton) {
                    $buttonName = $navButton['title'];
                    if (isset($navButton['auth']) && $navButton['auth'] === true) {
                        $data[] = [
                            'id' => menuId($moduleName,$groupTitle,$buttonName),
                            'moduleName' => $moduleName,
                            'groupTitle' => $groupTitle,
                            'buttonName' => $buttonName
                        ];
                    }
                }
            }
        }

        $builder = new ListBuilder();
        return $builder
            ->title("菜单可见管理")
            ->keyText("moduleName", "模块名")
            ->keyText("groupTitle", "菜单组名")
            ->keyText("buttonName", '菜单名')
            ->keyDoAction(urldecode(url('menuConfig', ['id' => '###'])), "编辑权限", false, "编辑权限")
            ->data($data)
            ->fetch();
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * @Auth(desc="菜单设置")
     */
    public function menuConfig(Request $request, $id) {
        $builder = new ListBuilder();
        $data = [];
        foreach (RoleMenuReadable::all(['menu_id'=>$id]) as $readable) {
            $roleId = $readable['role_id'];
            $data[] = Role::get($roleId);
        }


        return $builder
            ->title("可见此菜单的用户")
            ->buttonNew(url('menuReadableNew', ['id' => $id]))
            ->buttonHref('返回', url('menuManager'))
            ->keyText('name', '角色名')
            ->keyText('desc', '描述')
            ->keyDoAction(urldecode(url('menuAjaxDelRole', ['rid'=>'###', 'mid'=>$id])), '删除')
            ->data($data)
            ->fetch();
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * @Auth(desc="菜单添加角色")
     */
    public function menuReadableNew(Request $request, $id) {
        if ($request->isPost()) {
            if(RoleMenuReadable::get(['menu_id'=>$id, 'role_id'=>$request->post('role_id')])) {
                $this->error("此角色已经存在！");
            }

            $roleMenuReadable = new RoleMenuReadable();
            $roleMenuReadable['role_id'] = $request->post('role_id');
            $roleMenuReadable['menu_id'] = $id;

            if ($roleMenuReadable->validate(true)->save()) {
                $this->success("", url("menuConfig", ['id'=>$id]));
            }else {
                $this->error();
            }
        }else {
            $builder = new ConfigBuilder();

            $roleList = Role::all();
            $roleSelect = [];
            foreach($roleList as $role) {
                $roleSelect[$role['id']] = $role['name'];
            }


            return $builder
                ->title("添加角色可视")
                ->keySelect('role_id', '角色', $roleSelect)
                ->buttonSubmit()
                ->buttonBack()
                ->fetch();
        }
    }

    /**
     * @param $rid
     * @param $mid
     * @Auth(desc="删除菜单角色")
     */
    public function menuAjaxDelRole($rid, $mid) {
        if(RoleMenuReadable::destroy(['role_id'=>$rid,'menu_id'=>$mid])) {
            $this->success("删除成功");
        }else {
            $this->error("删除失败");
        }
    }

    /**
     * 权限管理
     */
    /**
     * @return mixed
     * @Auth(desc="权限管理")
     */
    public function authManager() {
        $moduleManager = new Module();
        $data = [];
        foreach ($moduleManager->getInstalledModuleList() as $moduleName => $module) {
            $class = "\\app\\{$moduleName}\\controller\\Admin";
            $reflectionClass = new ReflectionClass($class);

            foreach ($reflectionClass->getMethods() as $method) {
                try {
                    $anno = $method->getAnnotation("Auth");
                    $data[] = [
                        'id' => actionId($moduleName, $method->getMethodName()),
                        'module' => $moduleName,
                        'controller' => "Admin",
                        'action' => $method->getMethodName(),
                        'desc' => $anno->getValues()['desc']
                    ];
                }catch (ReflectionException $e) {

                }
            }
        }
        $builder = new ListBuilder();
        return $builder
            ->title("权限管理")
            ->keyText("module", "模块名")
            ->keyText("controller", "控制器")
            ->keyText("action", "行为")
            ->keyText("desc", "描述")
            ->keyDoAction(urldecode(url("authConfig", ['id'=>'###'])), "编辑", false)
            ->data($data)
            ->fetch();
    }

    /**
     * @param $id
     * @return mixed
     * @Auth(desc="安全设置")
     */
    public function authConfig($id) {
        $data = [];
        foreach (RoleAuth::all(['action_id'=>$id]) as $item) {
            $role = Role::get($item['role_id']);
            $data[] = $role;
        }

        $builder = new ListBuilder();
        return $builder
            ->title("拥有此行为权限的用户")
            ->buttonNew(url('roleAuthNew', ['id' => $id]))
            ->buttonHref('返回', url('authManager'))
            ->keyText('name', "角色名")
            ->keyText('desc', '描述')
            ->keyDoAction(urldecode(url('authAjaxDelRole', ['rid'=>'###', 'aid'=>$id])), '删除')
            ->data($data)
            ->fetch();
    }


    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * @Auth(desc="角色安全设置与添加")
     */
    public function roleAuthNew(Request $request, $id) {
        if ($request->isPost()) {
            if(RoleAuth::get(['action_id'=>$id, 'role_id'=>$request->post('role_id')])) {
                $this->error("此角色已经存在！");
            }

            $roleAuth = new RoleAuth();
            $roleAuth['role_id'] = $request->post('role_id');
            $roleAuth['action_id'] = $id;

            if ($roleAuth->validate(true)->save()) {
                $this->success("", url("authConfig", ['id'=>$id]));
            }else {
                $this->error();
            }
        }else {
            $builder = new ConfigBuilder();

            $roleList = Role::all();
            $roleSelect = [];
            foreach($roleList as $role) {
                $roleSelect[$role['id']] = $role['name'];
            }


            return $builder
                ->title("添加角色权限")
                ->keySelect('role_id', '角色', $roleSelect)
                ->buttonSubmit()
                ->buttonBack()
                ->fetch();
        }
    }

    /**
     * @param $rid
     * @param $aid
     * @Auth(desc="删除角色验证")
     */
    public function authAjaxDelRole($rid, $aid) {
        if(RoleAuth::destroy(['role_id'=>$rid,'action_id'=>$aid])) {
            $this->success("删除成功");
        }else {
            $this->error("删除失败");
        }
    }
}

