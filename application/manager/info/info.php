<?php
    return [
        'nav_side_bar_config' => [
            [
                'title' => '后台管理',
                'buttonList' => [
                    [
                        'auth' => true,
                        'title' => '后台管理',
                        'href' => \think\Url::build('manager/admin/backendManager'),
                    ],
                    [
                        'auth' => true,
                        'title' => '用户管理',
                        'href' => \think\Url::build('manager/admin/adminManager'),
                    ],
                    [
                        'auth' => true,
                        'title' => '角色管理',
                        'href' => \think\Url::build('manager/admin/roleManager'),
                    ],
                    [
                        'auth' => true,
                        'title' => '菜单可见管理',
                        'href' => \think\Url::build('manager/admin/menuManager'),
                    ],
                    [
                        'auth' => true,
                        'title' => '权限管理',
                        'href' => \think\Url::build('manager/admin/authManager')
                    ]
                ]
            ],
        ]
    ];