<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
use AppserverIo\Lang\Reflection\ReflectionAnnotation;
use think\Loader;

/**
 * 通过图片id获取图片数据模型
 * @param $id
 * @return static
 */
function get_img($id) {
    $img = new \app\admin\model\Img();
    return $img->getImg($id);
}

function get_file($id) {
    $file = new \app\admin\model\File();
    return $file->getFile($id);
}

/**
 * 通过图片id获取图片url
 * @param $id
 * @return string
 */
function get_img_url($id) {
    $img = get_img($id);
    if($img) {
        if($img->type == \app\admin\Model\Img::TYPE_LOCAL) {
            return ROOT_URL . $img->path;
        }
    }
    return "";
}
function pic($id) {
    $img = get_img($id);
    if($img) {
        if($img->type == \app\admin\Model\Img::TYPE_LOCAL) {
            return ROOT_URL . $img->path;
        }
    }
    return "";
}

function get_file_path($id) {
    $file = get_file($id);
    if($file) {
        if($file->type == \app\admin\model\File::TYPE_LOCAL) {
            return ROOT_URL . $file->path;
        }
    }
    return false;
}

/**
 * 图片数组转字符串，在ConfigBuilder接收到上传图片ids，保存ids的时候使用
 * @param $arr
 * @return string
 */
function img_array_to_string($arr) {
    return implode(",",$arr);
}
function file_array_to_string($arr) {
    return implode(",",$arr);
}

function string_to_img_array($str) {
    if($str) {
        return explode(',',$str);
    }else {
        return [];
    }

}
function string_to_file_array($str) {
    if($str) {
        return explode(",",$str);
    }else {
        return [];
    }

}

/**
 * 获取当前被选择的公众号(指的是后台被选中)
 * @return mixed
 */
function get_selected_mp() {
    return \app\admin\model\Mp::getSelectedMp();
}
function get_bind_mp() {
    $id = \app\common\model\Config::getValue('WEIPETER_MP_BIND');
    if($id) {
        $mp = \app\admin\model\Mp::get($id);
        if($mp) {
            return $mp->toArray();
        }
    }
    return null;
}


function all2Array($all) {
    foreach ($all as &$d) {
        $d = $d->toArray();
    }
    return $all;
}


class ErrorController extends \think\Controller {
    public function displayError($msg) {
        $this->error($msg);
    }
}
/**
 * 验证表单令牌
 */
function checkFormToken() {
    $controller = new ErrorController();
    $formToken = Loader::validate('FormToken');
    $request = \think\Request::instance();
    if(!$formToken->check($request->post())) {
        $controller->displayError($formToken->getError());
    }
}


/**
 * 计算action_id 时候使用
 * @param $str
 * @return int
 */
function hashCode64($str) {
    $str = (string)$str;
    $hash = 0;
    $len = strlen($str);
    if ($len == 0 )
        return $hash;

    for ($i = 0; $i < $len; $i++) {
        $h = $hash << 5;
        $h -= $hash;
        $h += ord($str[$i]);
        $hash = $h;
        $hash &= 0xFFFFFFFF;
    }
    return $hash;
}

class Auth extends ReflectionAnnotation {

    public function getDesc() {
        dump($this);
    }
}


function menuId($moduleName, $groupName, $buttonName) {
    return hashCode64("{$moduleName}/{$groupName}/{$buttonName}");
}

function actionId($moduleName, $methodName) {
    return hashCode64("{$moduleName}/Admin/{$methodName}");
}

function cmsConfig($name) {
    return \app\common\model\Config::getValue($name);
}