<?php
    namespace think\template\taglib;
    use think\template\TagLib;
    class Peter extends TagLib
    {
        protected $tags = array(
            'if' => array('attr' => 'exp', 'close' => 1, 'level' => 5),
            'elseif' => array('attr' => 'exp', 'close' => 0),
            'else' => array('close' => 0),
            'istrue' => array('attr' => 'var', 'close' => 1, 'level' => 5),

            'foreach' => array('attr' => 'exp', 'close' => 1, 'level' => 5),
            'for' => array('attr' => 'exp', 'close' => 1, 'level' => 5),
            'break' => array('close' => 0),
        );




        /*****************以下为逻辑标签*****************/


        /**
         * if标签解析
         * @access public
         * @param array $tag 标签属性
         * @return string
         * 模板运用示例：
         * <if exp="$abc == fun()">
         */
        public function tagIf($tag,$content) {
            $exp = $tag['exp'];
            $parseStr   =   '<?php if('.$exp.'): ?>'.$content.'<?php endif; ?>';
            return $parseStr;
        }
        public function tagIstrue($tag,$content) {
            $exp = $tag['var'];
            $parseStr   =   "<?php if(isset({$exp}) && {$exp}): ?>{$content}<?php endif; ?>";
            return $parseStr;
        }


        public function tagElseif($tag) {
            $exp = $tag['exp'];
            $parseStr   =   '<?php elseif('.$exp.'): ?>';
            return $parseStr;
        }

        public function tagElse($tag) {
            $parseStr = '<?php else: ?>';
            return $parseStr;
        }

        public function tagBreak($tag) {
            $parseStr = '<?php break; ?>';
            return $parseStr;
        }


        public function tagForeach($tag,$content) {
            $exp = $tag['exp'];

            $parseStr   =   '<?php foreach('.$exp.'): ?>';
            $parseStr  .=   $content;
            $parseStr  .=   '<?php endforeach; ?>';
            return $parseStr;
        }

        public function tagFor($tag,$content) {
            $exp = $tag['exp'];

            $parseStr = "<?php for(".$exp."){ ?>";
            $parseStr .= $content;
            $parseStr .= "<?php } ?>";
            return $parseStr;
        }
    }